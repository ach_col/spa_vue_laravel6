export default  {
    state: {
        user: {}
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        }
    },
    actions: {
        getUser({ commit }) {
            return new Promise((resolve) => {
                axios.get('/api/user').then((res) => {
                    commit('setUser', res.data);
                    resolve();
                }).catch((err) => {
                    console.log(err.response.data.message);
                });
            });
        },
        logoutUser({ commit }) {
            commit('setUser', {});
        }
    },
    getters: {
        fullNameUser(state) {
            return state.user.name;
        },
        getUserData(state) {
            return (_.isEmpty(state.user)) ? null : state.user;
        }
    }
}
