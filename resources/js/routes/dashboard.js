import Vue from "vue";

import Index from '@/js/pages/dashboard/index';

// Importamos middlewares
import authMiddleware from '@/js/middleware/auth';

export default [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Index,
        meta: {
            middleware: authMiddleware,
        },
    },
]
