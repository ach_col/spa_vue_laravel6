import Vue from 'vue';

import Home  from '@/js/pages/Home.vue';
import Error from '@/js/pages/404';

// Importamos middlewares
import guestMiddleware from '@/js/middleware/guest';

export default [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            middleware: guestMiddleware,
        },
    },
    {
        path: '*',
        name: '404',
        component: Error,
        meta: {
            middleware: guestMiddleware,
        },
    },
];
