// Middleware encargado de validar que la sesión exista
export default function auth({next, router}) {
    if (!localStorage.getItem('api_token')) {
        return router.push({name: 'home'});
    }
    return next();
}
