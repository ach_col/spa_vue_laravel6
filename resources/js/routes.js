import Vue from 'vue';
import VueRouter from 'vue-router';

// Importamos rutas web y dashboard
import WebRoutes from '@/js/routes/web';
import DashboardRoutes from '@/js/routes/dashboard';

import { nextFactory } from '@/js/conf/middlewareConf';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        ...WebRoutes,
        ...DashboardRoutes,
    ]
});

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware) ? to.meta.middleware : [to.meta.middleware];
        const context = {from, next, router, to};
        const nextMiddleware = nextFactory(context, middleware, 1);
        return middleware[0]({ ...context, next: nextMiddleware });
    }
    return next();
});

export default router;
