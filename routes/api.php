<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', function() {
    // Lo hacemos de esta manera ya que no crearemos una sesión
    // Verificar si el correo existe en la base de datos
    if ( $user = User::whereEmail(request('email'))->first() ) {

        // Verificar si la contraseña es válida
        if( Hash::check(request('password'), $user->password) ) {
            return response()->json([
                'message'    => 'Bienvenido@ ' . $user->name,
                'api_token'  => $user->api_token
            ], 200);
        }
    }
    return response()->json(['message'   => 'Tus credenciales no concuerdan con nuestros registros',
        'api_token' => null
    ], 401);
});

